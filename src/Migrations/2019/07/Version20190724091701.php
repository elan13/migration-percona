<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Migrations\Database;
use Doctrine\DBAL\Schema\Schema;
use Elan\PerconaSchemaChangeBundle\Migration\AbstractMigrationPercona;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190724091701 extends AbstractMigrationPercona
{
    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `dsd` MODIFY COLUMN `ds` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ""');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDatabase(): string
    {
        return 'dsd';
    }
}
