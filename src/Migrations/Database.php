<?php


namespace App\Migrations;


final class Database
{
    const  AWS = 'aws';
    const  BOOKKEEPING = 'bookkeeping';
    const  DOCFLOW = 'docflow';
    const  LOGS = 'logs';
    const  PARSER = 'parser';
    const  FRONTEND = 'pkw_frontend';
    const  PKW = 'pkwteile_de';
    const  STORAGE = 'pkwteile_store';
    const  SHOP = 'SHOP';
    const  TECDOC = 'tecdoc_new';
}
